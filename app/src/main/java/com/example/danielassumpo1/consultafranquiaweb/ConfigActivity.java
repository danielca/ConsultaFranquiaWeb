package com.example.danielassumpo1.consultafranquiaweb;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

public class ConfigActivity extends AppCompatActivity {

    EditText diaFinal;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        int dataFinal = sharedPref.getInt(getString(R.string.dias), 1);

        diaFinal = (EditText) findViewById(R.id.diaConfig);
        diaFinal.setText(""+dataFinal);
        diaFinal.setFilters(new InputFilter[]{ new InputFilterMinMax("1", "31")});


    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        //replaces the default 'Back' button action
        if(keyCode==KeyEvent.KEYCODE_BACK)   {
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putInt(getString(R.string.dias), Integer.parseInt(diaFinal.getText().toString()));
            editor.apply();
            editor.commit();
            finish();
        }
        return super.onKeyDown(keyCode, event);

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt(getString(R.string.dias), Integer.parseInt(diaFinal.getText().toString()));
                editor.apply();
                editor.commit();
                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}

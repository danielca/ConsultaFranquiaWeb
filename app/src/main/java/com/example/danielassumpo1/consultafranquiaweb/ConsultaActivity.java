package com.example.danielassumpo1.consultafranquiaweb;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;

import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import android.widget.TextView;

import com.llollox.androidprojects.wdatelibrary.WDate;
import com.timqi.sectorprogressview.ColorfulRingProgressView;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;
import com.yarolegovich.lovelydialog.LovelyTextInputDialog;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class ConsultaActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    SwipeRefreshLayout swipeLayout;
    String franquia = "", consumida = "";
    String porCento = "";
    String tipoFranquia = "";
    int dataFinal;
    String LINK = null;
    private TextView textoConsumido, textoFranquia, textoData, nomeFranquia, textoPorCento;
    ColorfulRingProgressView progresso;
    Snackbar erro3g, erroGenerico, erroPrimeiraVez;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_main_layout);
        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeColors(Color.BLACK, Color.RED);


        textoConsumido = (TextView) findViewById(R.id.textoConsumido);
        textoFranquia = (TextView) findViewById(R.id.textoFranquia);
        textoData = (TextView) findViewById(R.id.textoFimPlano);
        nomeFranquia = (TextView) findViewById(R.id.labelFranquia);
        textoPorCento = (TextView) findViewById(R.id.textoPorCento);

        progresso = (ColorfulRingProgressView) findViewById(R.id.progresso);

        erro3g = Snackbar.make(findViewById(R.id.myCoordinatorLayout), R.string.erro_3G,
                Snackbar.LENGTH_INDEFINITE);
        erro3g.setAction(R.string.ok, new View.OnClickListener() {
            public void onClick(View v) {
                erro3g.dismiss();
            }
        });

        erroPrimeiraVez = Snackbar.make(findViewById(R.id.myCoordinatorLayout), R.string.erro_3G,
                Snackbar.LENGTH_LONG);
        erroPrimeiraVez.setAction(R.string.ok, new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });


        erroGenerico = Snackbar.make(findViewById(R.id.myCoordinatorLayout), R.string.erro_generico,
                Snackbar.LENGTH_LONG);
//        erroGenerico.setAction(R.string.ok, new View.OnClickListener() {
//            public void onClick(View v) {
//                erroGenerico.dismiss();
//            }
//        });


        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        String defaultValue = getString(R.string.valor_franquia_default);
        franquia = sharedPref.getString(getString(R.string.valor_franquia), defaultValue);
        defaultValue = getString(R.string.valor_consumido_default);
        consumida = sharedPref.getString(getString(R.string.valor_consumido), defaultValue);
        defaultValue = getString(R.string.tipo_franquia_default);
        tipoFranquia = sharedPref.getString(getString(R.string.tipo_franquia), defaultValue);

        dataFinal = sharedPref.getInt(getString(R.string.dias), 1);


        LINK = sharedPref.getString(getString(R.string.link_pessoal), null);

        if (LINK == null) {
            primeiraVez();
        }

        botaNaTela();

    }

    protected void onResume() {
        swipeLayout.setRefreshing(false);
        botaNaTela();
        super.onResume();
    }

    protected void primeiraVez() {

        NetworkInfo info = Connectivity.getNetworkInfo(this);
        if (info != null && info.isConnected() && info.getType() != ConnectivityManager.TYPE_WIFI) {
            final LovelyProgressDialog aguarde = new LovelyProgressDialog(this)
                    .setIcon(R.drawable.cellphone)
                    .setTitle(R.string.bem_vindo)
                    .setMessage(R.string.aguarde)
                    .setTopColorRes(R.color.primary);


            final LovelyTextInputDialog data = new LovelyTextInputDialog(this)
                    .setTopColorRes(R.color.primary)
                    .setTitle(R.string.bem_vindo)
                    .setMessage(R.string.digite_data)
                    .setIcon(R.drawable.calendar_clock)
                    .setCancelable(false)
                    .setInputType(InputType.TYPE_CLASS_NUMBER)
                    .setInputFilter(R.string.erro_entrada_data, new LovelyTextInputDialog.TextFilter() {
                        @Override
                        public boolean check(String text) {
                            if (!"".equals(text)) {
                                int dia = Integer.parseInt(text);
                                return dia >= 1 && dia <= 31;
                            }
                            return false;

                        }
                    })
                    .setConfirmButton(android.R.string.ok, new LovelyTextInputDialog.OnTextInputConfirmListener() {
                        @Override
                        public void onTextInputConfirmed(String text) {
                            dataFinal = Integer.parseInt(text);
                            fimPrimeiraVez();
                        }
                    })
                    .setConfirmButtonColor(R.color.primary);


            aguarde.show();
            final WebView pagina = (WebView) findViewById(R.id.webView1);
            pagina.setVisibility(View.GONE);
            pagina.getSettings().setJavaScriptEnabled(true);
            pagina.getSettings().setLoadsImagesAutomatically(true);
            pagina.loadUrl("http://consumo.claro.com.br");
            pagina.setWebViewClient(new WebViewClient() {
                public void onPageFinished(WebView view, String url) {
                    LINK = pagina.getUrl();
                    aguarde.dismiss();
                    data.show();
                }
            });
        } else {
            erroPrimeiraVez.show();
            swipeLayout.setRefreshing(false);
        }
    }

    void fimPrimeiraVez() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.link_pessoal), LINK);
        editor.putInt(getString(R.string.dias), dataFinal);
        editor.apply();
        editor.commit();
        botaNaTela();
    }

    Call readWebpage(Callback callback) {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(LINK)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
        return call;

    }

    public void botaNaTela() {
        final float porCentoValor;
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        if (franquia.contains("GB") && !consumida.contains("GB")) {
            porCentoValor = Float.parseFloat(df.format((Float.parseFloat(consumida.replaceAll("[^0-9.]", "")) / (Float.parseFloat(franquia.replaceAll("[^0-9.]", "")) * 1000)) * 100).replace(",", "."));

        } else {
            porCentoValor = Float.parseFloat(df.format((Float.parseFloat(consumida.replaceAll("[^0-9.]", "")) / Float.parseFloat(franquia.replaceAll("[^0-9.]", ""))) * 100).replace(",", "."));
        }

        WDate today = new WDate();
        WDate endDate = new WDate();
        endDate.setDay(dataFinal);
        if(dataFinal <= today.getDay()){
            endDate.addMonths(1);
        }

        long diasFaltando = endDate.getNumDaysBetween(today.getDate());

        final String diasFaltandoString = diasFaltando + " " + getString(R.string.dias);

        porCento = porCentoValor + "%";
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                textoFranquia.setText(franquia);
                textoConsumido.setText(consumida);
                textoData.setText(diasFaltandoString);
                progresso.setStartAngle(0);
                progresso.setPercent(porCentoValor);
                textoPorCento.setText(porCento);
                nomeFranquia.setText(tipoFranquia);
                swipeLayout.setRefreshing(false);

            }
        });

    }

    public void atualizou(String bruto) {
        ArrayList<String> qntdPorcentagens = new ArrayList<>();
        ArrayList<String> qntdFranquias = new ArrayList<>();


        Document doc = Jsoup.parse(bruto);
        Elements percentos = doc.select("span.new-indicator");
        Elements franquias = doc.select("div.text-right");
        float valorFranquia = 0;
        String valorFranquiaString = "";
        float qntConsumida = 0;
        String qntConsumidaString = "";

        for (Element percento : percentos) {
            qntdPorcentagens.add(percento.text());
        }
        for (Element franquia : franquias) {
            qntdFranquias.add(franquia.text());
        }

        if (!(qntdFranquias.size() == 0) && qntdFranquias.size() == qntdPorcentagens.size() && !(qntdFranquias.size() == 0)) {
            tipoFranquia = "Franquia:";
            float varConta;
            for (int i = 0; i < qntdFranquias.size(); i++) {
                if (qntdFranquias.get(i).contains("GB")) {
                    varConta = Float.parseFloat(qntdFranquias.get(i).replaceAll("[^0-9.]", "")) * 1000;
                    valorFranquia += varConta;
                    qntConsumida += (varConta * Float.parseFloat(qntdPorcentagens.get(i).replaceAll("[^0-9.]", ""))) / 100;

                } else {
                    varConta = Float.parseFloat(qntdFranquias.get(i).replaceAll("[^0-9.]", ""));
                    valorFranquia += varConta;
                    qntConsumida += (varConta * Float.parseFloat(qntdPorcentagens.get(i).replaceAll("[^0-9.]", ""))) / 100;
                }
            }
            if (valorFranquia >= 1000) {
                valorFranquiaString = (valorFranquia / 1000) + "GB";
            } else {
                valorFranquiaString = (valorFranquia) + "MB";
            }

            if (qntConsumida >= 1000) {
                qntConsumidaString = (qntConsumida / 1000) + "GB";
            } else {
                qntConsumidaString = (qntConsumida) + "MB";
            }
        } else {
            erroGenerico.show();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    swipeLayout.setRefreshing(false);
                }
            });

            return;
        }

        franquia = valorFranquiaString;
        consumida = qntConsumidaString;

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.valor_franquia), franquia);
        editor.putString(getString(R.string.valor_consumido), consumida);
        editor.putString(getString(R.string.tipo_franquia), tipoFranquia);
        editor.apply();
        editor.commit();

        botaNaTela();
    }

    @Override
    public void onRefresh() {
        atualiza();
    }

    public void atualiza() {
        NetworkInfo info = Connectivity.getNetworkInfo(this);
        if (LINK == null) {
            primeiraVez();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    swipeLayout.setRefreshing(false);
                }
            });
            return;
        }

        if (info != null && info.isConnected() && info.getType() != ConnectivityManager.TYPE_WIFI) {
            readWebpage(new Callback() {

                @Override
                public void onFailure(Call call, IOException e) {
                    System.err.print(e.getMessage());
                    erroGenerico.show();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            swipeLayout.setRefreshing(false);
                        }
                    });                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (response.isSuccessful()) {
                        atualizou(response.body().string());
                    } else {
                        erroGenerico.show();

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                swipeLayout.setRefreshing(false);
                            }
                        });
                    }
                }
            });

        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    swipeLayout.setRefreshing(false);
                }
            });            erro3g.show();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_consulta, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.config) {
            Intent configIntent = new Intent(this, ConfigActivity.class);
            startActivity(configIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
